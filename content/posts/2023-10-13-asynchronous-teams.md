Title: Asynchronous Teams
Date: 2023-10-13
Category: Teams, Remote Work
Tags: Remote Work, Organization
Slug: asynchronous-teams
Status: draft

For the past few years I've been working at fully remote startup. All team members have been working remotely, some in different timezones.

It has been a turbulent journey, and I will try to summarize my experience in this post.

# Leadership

Must be clear WHY to keep people focused and motivated. 

Clear tasks that can be performed independently



Clear tasks

Everyone has to be on the same page




# Asynchronous Communication

Asynchronous communication migth be the most common way of communicating now days. In the setting of asychronous teams it is the main way of commincating.

The clear advantage of asychronous communication is that work can continue uninterrupted for longer periods. 
I for one will sometimes turn off all notifications and distractions when I know the problem I'll be working on needs my complete focus.

It comes as no suprise that the advantage of working uninterrupted means that team members waiting for your reply will be waiting longer.
So the biggest advantage of asychronous commincattion is also the biggest disadvantage, depending on what end of the communication you are.

To ensure asychronous communication does not effect the productivity of your team members it is important to be able to do context switching.
Just like in a computer, if your thread is waiting for a slow processes, work will stall. 
Therefore it is important that the team members have multiple tasks to work on. The tasks must be of varied nature, so that context switching is worht while. 

This brigns us back to leadership. It is important the leadership can identify and assign tasks to enable context switching.



# Synchronous Communication

Eventhough Asynchronous commincatiion should be the main communicatio nchannel for asychronous teams, synchronisation is essential. 

Daily standups can be ignored. It won't work for asychronous teams.  What we found work well is weekly meetings where issues can be discussed. 
Views can be synchronised etc.

in addition to standing meeting there should an additional synchronisation point for reviewing issues and assigning them.

Only berörda pars should be invloved in these meetings. A good Idea coulbe be to have standing meetings in the middle of the week. 
However it should be ok to skip these meetings if the members don't see any point in having them


# In Person Meetings

Eventhough Asynchronous and synchronisation commincatiion goes a long way there will be need for in person meetings. 

Some tasks just don't work remotly for example workshops and brainstormign. 
You need quicker coomunication and it must be possible to listen in to all members and read their bodylanguage.


# Conclusion